import os
import subprocess as sp
import time as t
import json as js
import shlex as sh

# CLASS DEFINITIONS

class c:
    HEADER = '\033[95m'
    INFO = '\033[94m'
    OK = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    ENDC = '\033[0m'

class p:
    CREDS = 'TEST SUITE BY ANTOINE DRAY'.center(80, ' ')
    OK = '[  ' + c.OK + 'OK' + c.ENDC + '  ] '
    FAIL = '[' + c.FAIL + 'FAILED' + c.ENDC + '] '
    PADDING = '\n' + '-' * 80 + '\n'
    END = 'END'.center(80, ' ')

# ! CLASS DEFINITIONS

# CONSTANTS DEFINITION

cmd_file = './tests/tests.json'
os.environ["LD_PRELOAD"] = "./libmalloc.so"

# ! CONSTANTS DEFINITIONS

# Start message
print(p.PADDING + p.CREDS + p.PADDING)

# Starting timer
start_t = t.time()

# Runing test
f = open(cmd_file, 'r')
json_data = js.load(f)
nb_tests = 0
fail = 0
ok = 0
for cmds in json_data['cmds']:

    f_cmds = sh.split(cmds)
    my_run = sp.Popen(f_cmds, stdout=sp.PIPE, stderr=sp.PIPE, env=os.environ)
    chk_run = sp.Popen(sh.split(cmds), stdout=sp.PIPE, stderr=sp.PIPE)
    my_out, my_err = my_run.communicate()
    chk_out, chk_err = chk_run.communicate()
    if my_out == chk_out:
        print(p.OK + cmds)
        ok += 1
    else:
        print(p.FAIL + cmds)
        fail += 1
    nb_tests += 1

# Printing stats
print(p.PADDING)

p_nb_tests = c.INFO + str(nb_tests) + c.ENDC
p_time = c.INFO + "{0:.5f}".format(t.time() - start_t) + c.ENDC
print("Ran " + p_nb_tests + " tests in " + p_time + "s")

print("\nTest passed: " + c.OK + str(ok) + c.ENDC)
print("Test failed: " + c.FAIL + str(fail) + c.ENDC)

rate = (ok / nb_tests) * 100
p_rate = "{0:.0f}".format(rate) + "%"
print("Success rate: " + c.BOLD + (c.FAIL, c.OK)[rate >= 50] + p_rate + c.ENDC)

# End message
print(p.PADDING + p.END + p.PADDING)