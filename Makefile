# Makefile

rwildcard=$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))

UNAME_S := $(shell uname -s)
CC :=
ifeq ($(UNAME_S),Linux)
	CC += gcc
endif
ifeq ($(UNAME_S),Darwin)
	CC += gcc-8
endif

CFLAGS = -Wall -Wextra -Werror -pedantic -std=c99 -fPIC -fvisibility=hidden -g -fno-builtin
LDFLAGS = -shared

TARGET_LIB = libmalloc.so
SRC = $(call rwildcard, , *.c)
OBJS = ${SRC:.c=.o}

.PHONY: all ${TARGET_LIB} clean check

${TARGET_LIB}: ${OBJS}
	${CC} ${LDFLAGS} -o $@ $^

all: ${TARGET_LIB}

check: ${TARGET_LIB}
	python3 tests/test.py

clean:
	${RM} ${TARGET_LIB} ${OBJS}

# END
