#define _GNU_SOURCE
#include <string.h>
#include <stdint.h>
#include "link.h"

void *calloc(size_t nmemb, size_t size)
{
    void *p = malloc(nmemb * size);
    if (!p)
        return NULL;
    if (p)
        memset(p, 0, size * nmemb);
    return p;
}