#define _GNU_SOURCE
#include <stdint.h>
#include "link.h"

#define NUM_THREADS 4

struct global_data g_data;

static inline void *add_block(size_t size)
{
    if (ccast(g_data.cur) + size + META_SIZE > ccast(g_data.end))
        add_page();
    char *new = g_data.cur;
    flcast(new)->size = size + META_SIZE;
    flcast(new)->next = NULL;
    flcast(new)->prev = NULL;
    g_data.cur = incr_ptr(new, size + META_SIZE);
    return incr_ptr(new, META_SIZE);
}

static inline void *big_malloc(size_t size)
{
    void *addr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE
                            | MAP_ANONYMOUS, -1, 0);
    if (addr == MAP_FAILED)
        return NULL;
    flcast(addr)->size = size;
    return incr_ptr(addr, META_SIZE);
}

void *malloc(size_t size)
{
    if (!g_data.start)
        add_page();
    size = word_align(size + META_SIZE);
    if (size > g_data.page_size)
        return big_malloc(size);
    if (!g_data.free_list)
        return add_block(size);
    if (g_data.free_list && g_data.max_free > size)
    {
        struct free_list *p = g_data.free_list;
        for (; p; p = p->next)
        {
            if (p->size >= size)
            {
                struct free_list *prev = p->prev;
                struct free_list *next = p->next;
                if (prev)
                    prev->next = next;
                else
                    g_data.free_list = next;
                if (next)
                    next->prev = prev;
                else
                    p->next = NULL;
                return ccast(p) + META_SIZE;
            }
        }
    }
    return add_block(size);
}