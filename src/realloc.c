#define _GNU_SOURCE
#include <string.h>
#include "link.h"

static inline size_t get_page_mult(size_t n)
{
    size_t mult = g_data.page_size;
    while (n > mult)
    {
        mult += g_data.page_size;
    }
    return mult;
}

void *realloc(void *ptr, size_t size)
{
    if (!ptr)
        return malloc(size);

    if (!size)
    {
        free(ptr);
        return malloc(0);
    }
    struct free_list *p = get_meta(ptr);

    if (size < p->size)
        return ptr;
    if (size + META_SIZE > g_data.page_size)
    {
        size_t aligned_size = get_page_mult(size + META_SIZE);
        void *new = mremap(p, p->size, aligned_size, MAP_PRIVATE
                                | MAP_ANONYMOUS);
        if (new == MAP_FAILED)
            return ptr;
        flcast(new)->size = aligned_size;
        free(ptr);
        return incr_ptr(new, META_SIZE);
    }
    else
    {
        void *new = malloc(size);
        if (!new)
            return NULL;
        size_t min = size > p->size ? p->size : size;
        new = memcpy(new, ptr, word_align(min));
        free(ptr);
        return new;
    }
}