#include "link.h"

void free(void *ptr)
{
    if (!ptr)
        return;
    struct free_list *p = get_meta(ptr);
    if (!p)
        return;
    if (p->size > g_data.page_size)
    {
        munmap(ptr, p->size);
        return;
    }
    if (!g_data.free_list)
    {
        g_data.max_free = p->size;
        g_data.free_list = cast(p);
        return;
    }
    if (p->size > g_data.max_free)
        g_data.max_free = p->size;
    if (p->next)
        p->next = g_data.free_list;
    if (p->prev)
        p->prev = NULL;
    if (g_data.free_list->prev)
        g_data.free_list->prev = p;
    g_data.free_list = p;
}