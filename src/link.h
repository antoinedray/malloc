#ifndef LINK_H
#define LINK_H

#include <stdio.h>
#include <pthread.h>
#include <sys/mman.h>

#define META_SIZE sizeof(struct free_list)

struct global_data
{
    size_t page_size;
    void *start;
    void *end;
    void *cur;
    size_t max_free;
    struct free_list *free_list;
    pthread_mutex_t lock_x;
};

extern struct global_data g_data;

struct free_list
{
    size_t size;
    struct free_list *next;
    struct free_list *prev;
};

__attribute__((visibility("default"))) void *malloc(
    size_t __attribute__((unused)) size);

__attribute__((visibility("default"))) void free(
    void __attribute__((unused)) *ptr);

__attribute__((visibility("default"))) void *realloc(
    void __attribute__((unused)) *ptr, size_t __attribute__((unused)) size);

__attribute__((visibility("default"))) void *calloc(
    size_t __attribute__((unused)) nmemb,
    size_t __attribute__((unused)) size);

/* AUXILARY FUNCTIONS */

__attribute__((visibility("hidden"))) void *cast(void *p);
__attribute__((visibility("hidden"))) char *ccast(void *p);
__attribute__((visibility("hidden"))) struct free_list *flcast(void *p);
__attribute__((visibility("hidden"))) struct big_meta *bmcast(void *p);
__attribute__((visibility("hidden"))) size_t word_align(size_t n);
__attribute__((visibility("hidden"))) struct free_list *get_meta(void *p);
__attribute__((visibility("hidden"))) void *incr_ptr(void *p, size_t n);
__attribute__((visibility("hidden"))) void *get_page(void);
__attribute__((visibility("hidden"))) void add_page(void);

/* ! AUXILARY FUNCTIONS */

#endif /* ! LINK_H */