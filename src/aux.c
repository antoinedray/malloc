#define _GNU_SOURCE
#include <unistd.h>
#include "link.h"

struct global_data g_data;

void *cast(void *p)
{
    return p;
}

char *ccast(void *p)
{
    return p;
}

struct free_list *flcast(void *p)
{
    return p;
}

struct big_meta *bmcast(void *p)
{
    return p;
}

size_t word_align(size_t n)
{
    return (n + sizeof(size_t) - 1) & ~(sizeof(size_t) - 1);
}

struct free_list *get_meta(void *p)
{
    return flcast(ccast(p) - META_SIZE);
}

void *incr_ptr(void *p, size_t n)
{
    char *tmp = ccast(p);
    tmp += n;
    return tmp;
}

void *get_page(void)
{
    g_data.page_size = sysconf(_SC_PAGESIZE);
    void *addr = mmap(NULL, g_data.page_size, PROT_READ | PROT_WRITE,
        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (addr == MAP_FAILED)
        return NULL;
    return addr;
}

void add_page(void)
{
    g_data.start = get_page();
    g_data.cur = g_data.start;
    g_data.end = ccast(g_data.start) + g_data.page_size;
}